# The Handbook

The following website is one of the official PBST handbooks, written by the Trainers for all PBST members to easily read and understand. (This site is programmed and developed by superstefano4/Stefano). 
Please read it carefully.

Failure to follow the handbook may result in punishment(s), ranging from a simple warning to a demotion or even blacklist.
Any changes to this handbook will be made **clear.**

# Reminders to all PBST members
  * Please abide by the [ROBLOX Community Rules](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) **at all times.**
  * Be respectful to your fellow players, don’t act like you’re the boss of the game.
  * Follow the orders of anyone who outranks you.
  * We are not at war with Innovation Inc., don’t be hostile to them in any way.
  * Do not try to evade punishment in any way. Doing so will only result in you receiving harsher punishments.
  * PBCC has an anti-spawnkill protection in place, which makes it impossible to kill anyone at the spawn area. This protection extends through the shop and stairwell, and ends past the yellow line at the bottom of the elevator.
    – Should you discover your weapon does allow you to kill at spawn, **don’t spawnkill. This overrides any rules established elsewhere in the handbook.**
    – Camping right outside the spawn border and blocking it is also considered spawnkilling.
  * Killing large amounts of people randomly (mass random killing) is always forbidden, on-duty or not. Use the !call command to call PIA to your assistance if someone is mass random killing.
  * Trainers will be exempt from most rules in the handbook, except all the former reminders.

## Going on duty to patrol
Before you can do your job as Pinewood Security, you need to show to civilians and other PBST members that you are on duty as Security. To start your duty as security, you have to wear a uniform and have your Kronos ranktag be set to Security. Having only one of these will not make you count as on-duty, and you won’t be allowed to take PBST Weapons.

This image is made by the **Trainer [EquilibriumCurse](https://www.roblox.com/users/962201589/profile)**.  
It shows all the requirements you need to have to be ``On Duty``!
![img](/PBST-On-Duty.png)

### Uniform
The official PBST uniforms can be found in the [store](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store).
All facilities have a shopping cart button on your screen where you can buy these uniforms as well. 
Buying a uniform from one of these methods and equipping your Roblox avatar with it is the most convenient way to get an official unifrom.

If you have no robux, most facilities are equipped with uniform givers to get you going. 
It’s also possible to buy donor commands and use the ```!shirt``` & ```!pants``` commands that come with that to get an official uniform.

#### Uniform Giver locations:
* [Pinewood Computer Core:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core)  
  You can access it from the main spawn through the lobby, up the elevator (follow ‘Security Sector + Cafe)’, in the PBST room to the right.
* [Pinewood Research Facility:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility)  
  You can access it from the main spawn into the room marked ‘PB SEC’, to the hallway between the baton giver and the coffee machine, changing room on the left.
* [Pinewood Builders HQ:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ)  
  You can access it on 4th floor, from elevators to the glass doors on the left, blue doors on the left.
* [PBST Training Facility:](https://www.roblox.com/games/298521066/PBST-Training-Facility)  
  You can access it at the right side of the spawn building.
* [PBST Activity Center:](https://www.roblox.com/games/1564828419/PBST-Activity-Center)  
  You can access it into the main hallway of the lobby.

Wearing a PET Fire or Hazmat uniform is allowed, but only if an official PBST Uniform is worn under it. 
The Medical Suit is not allowed because unlike the other two, this one actually changes your shirt & pants, making it impossible to wear a PBST uniform with it.

### Ranktag
A ranktag is the small text above your avatar in Pinewood facilities. 
This will be set to Security by default if you're a member of the Pinewood Security Builders Team. 
If it’s off, you can use the command ``!ranktag on`` to turn your ranktag on. If it’s set to another group than Security, use ``!setgroup PBST`` to get it set to Security so you can go patrol.

As a PBST Officer on duty, you are to protect the facility from various types of threats that may arise, like meltdowns, raiders, and OP Weapon users. 
When you have to evacuate, try your hardest to help the others.

### Going off duty 
To go off-duty, you have to visit the Security room and remove your uniform and PBST weapons. Reset character if required. 
It is highly recommended you remove your Security ranktag as well, by using the command ``!setgroup PB/PET/PBQA...`` to change to any other Pinewood group you’re in, or use the!ranktag off command to turn the ranktag off entirely.
If you switch to groups such as PET or TMS, you must follow their handbook and rules.

### Usage of weapons 
All PBST members are expected to use their weapons responsibly when on duty. If you find somebody breaking a rule, you have to give this person a warning. If he/she doesn’t listen to your warning, you are allowed to kill them. If a visitor has been killed and still breaks a rule, you can kill that person without needing to warn them again.

If a visitor is attempting to melt or freeze the core by changing one of the core controls, give one warning then shoot if he/she doesn't listen to the warning..
If he/she attacks you with OP Weapons or other acquired gear they may have, you can kill them without needing to warn them first. However, you cannot ‘revenge kill’, i.e. killing someone just because they killed you.

If you see anyone using OP Weapons against visitors, you can intervene and stop the attacker. If you require help fighting against the attacker, you may use the !call command if you're a tier 1 and above and call for PBST backup.

Loadouts for PBST members can be found close to the uniform givers, other givers may be spread throughout the facility. Every PBST member has access to the standard baton, and Tiers receive additional weapons from the blue loadouts. 
**Do not hand out any weapons to lower ranks**, this extends to the togglable lethal weapon givers that some facilities have. The only exception is when a T4+ asks you to do this.

These PBST weapons **may not** be abused in any way (such as randomly killing people without reason, etc). 
Cadets abusing PBST Weapons are to be warned (read the [Rulebreakers](/#how-to-deal-with-rulebreakers) chapter). In the event that you find a Tier abusing PBST Weapons, collect evidence and report to an HR by using the !call command and submitting a message to the PIA.

Be sure to have your uniform on when taking PBST weapons, for you are considered off-duty without uniform and you may not use PBST Weapons off-duty. 
PBST weapons may **never** be used to cause a melt-or freezedown. However, **on very rare occassions**, should a reverse-raid happen where TMS and PBST switch roles, you are exempt from this rule.

If you are using non-PBST weapons on duty (like the OP Weapons gamepass, or the pistol acquired from PBCC Credits or randomly spawned), you have to follow the same rules as if they were PBST Weapons.

In case of an emergency, a Tier 4 or higher may grant you permission to restrict a room to **PBST only**. 
In that case, you can kill anyone who tries to enter and only let on-duty PBST members in.

When off-duty, you may only use non-PBST Weapons, though you have more freedom with your usage. 
You can, for example, restrict rooms without needing permission, though excessive room restriction may count as mass random killing, which is always forbidden.

### How to deal with rulebreakers
Visitors who break a small rule are usually given a warning. You have to give at least **one** warning, after that you can kill them on the second warning. If a visitor has been killed after two warnings and they still break a rule, you can kill that person without needing to warn them again.

If a visitor is attempting to melt or freeze the core by changing one of the core controls, give a warning. If their response is hostile, you can kill them.

If a visitor with OP Weapons or otherwise acquired gear attack Security directly, this person may be killed without any warning. 
However, you cannot ‘``revenge kill``’, i.e. killing someone just because they killed you. If they break another rule again, you may kill them again (no need to use warnings at this point).

If a Security member is found breaking a rule in the handbook, alert them of their wrongdoing and give them a warning. 
If said Security member doesn’t listen or actually uses their weapons irresponsibly, kill them.

If a **Tier** is being abusive with their weapons, send a PIA call to deal with them.

### Kill on Sight (KoS)
The right to put somebody on KoS for PBST is reserved for Tier 4 and higher. 
On-duty PBST may not put anyone on KoS unless permission has been granted by a Tier 4+.
PIA can also put somebody on KoS, which is server-wide and extends to non-PBST.

Of course, the only exception of this rule are TMS and mutants (PBCC), which are to be killed on sight regardless.

## Important facilities to patrol
### Pinewood Computer Core
The Pinewood Computer Core is the most important place to patrol. Your main objective here is to prevent the core from melting or freezing. The following chart shows you how different settings affect the temperature: 
![img](https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png)

If your efforts fail and a meltdown does occur, you can go to the Emergency Coolant at Sector G to try to rescue the core. 
Use the Security Code to get in (the code that’s announced in-game). Keep the e-coolant in balance until the timer hits 0.

Aside from melt- and freezedown, PBCC also features various ‘disasters’, some of which require PBST to evacuate the visitors. 
In the event of a plasma surge, earthquake or radiation flood, bring the people in danger to safety.

Mutants also often appear at PBCC, if somebody steps into the pool of leaking radioactive fluid they become a mutant. 
Mutants are to be killed at once. **PBST members may not become mutants while on duty.**

Trains and other vehicles can be flown to places they shouldn’t be at. If you see someone flying a vehicle, you can use your taser to stop them. Remove any vehicle that has been flown.

#### TPS Calculator
For this version of the handbook, there exists a calculator. Used to calculate how many **T**emperature **P**er **S**econd goes up or down.
<Calculator />

### PBST Activity Center (PBSTAC)
The PBST Activity Center is the main facility for trainings and various other activities. You can practice one of the few obbies like the Gamma Obby, the Vortex of Rage or the Tall Towers. It also features a simulation of the core at PBCC with all the variables to change the temperature, and it will melt- or freeze down too.

PBSTAC can be visited by Raiders, who will kill mercilessly with their OP Weapons. They are KoS and patrolling PBST members have to join forces to keep them at bay.

Also be on the lookout for monsters like zombies, skeletons, or worse…

## Other groups you may encounter
### Pinewood Emergency Team
The Pinewood Emergency Team (PET) is set up as an emergency response group to a few types of emergencies at Pinewood facilities. They often help PBST in our mission to keep them safe. If no PET members are on-site and a fire or radiation spill needs to be dealt with at once, PBST is allowed to help put out the fire or stop the spill.

PET has 3 subdivisions: 
Medical, Fire & Hazmat, each with their own uniform. 
The Fire & Hazmat uniforms allow you to wear a PBST uniform underneath it, the Medical uniform doesn’t (see the [Uniform](/#Uniform) chapter).

You can switch between being on-duty for PBST or PET by using the !setgroup command. PET has its own set of tools, and you may not use PET tools when on-duty for PBST, and vice versa.

Read the PET handbook for more details on how PET works: 
- [PET - DevForum Handbook](https://devforum.roblox.com/t/pinewood-emergency-team-handbook/507807)
- [PET - Website Handbook](https://pet.pinewood-builders.com)

### The Mayhem Syndicate
The Mayhem Syndicate (TMS) is the opposite of PBST. 
Where we intend to protect the core, they intend to melt or freeze it. 
You can recognize them by the red ranktag. 
They often join a game in a raid, and when this happens you are advised to call for backup.

TMS members are Kill on Sight (KoS) by default. 
The only place where TMS may not be killed (aside from spawn) is at the TMS loadouts near the cargo trains. 
You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

As with all armed hostiles, you are allowed to fight back if TMS attacks you, even if they seem to be on their way to get their loadouts.

If you are off-duty when TMS starts to raid and you want to participate, you have to pick a side and fight on that side until the raid ends. 
**Do not change sides mid-raid**. This also applies if you are neutral or off-duty and decide to participate anyway. 
Beware that choosing to side for either TMS or PBST in a raid might get you put on KoS by the other side.

If TMS has achieved a melt- or freezedown, you may find them helping out the civilians to escape in the rockets. During these so-called “Yes-Survivor Runs”, you cannot attack them.

TMS also has its own handbook for more information on how they operate: 
- [The Mayhem Syndicate Handbook](https://devforum.roblox.com/t/the-mayhem-syndicate-handbook/595758/) (Official handbook)
- [The Mayhem Syndicate Handbook](https://tms.pinewood-builders.com/) (Unofficial handbook)

## Trainings and ranking up
### Training
::: warning NOTE:
Instructions in training are given in English, so understanding English is required.
:::

Tier 4+ can host a training where you can earn [points]((../ranks-and-ranking-up/)). The schedule of these can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). 

**Training Rules**:
* Listen to the host and follow his/her orders.
* Do not take your loadouts, the host will give you what you need in every activity.
* Always wear a PBST uniform in the training.
* Trainers may activate Permission to Speak (PTS), which means chatting will get you punished. Chat ‘``;pts request``’ to get permission if this is on.
* Some are marked as Disciplinary Training, which is stricter and more focused on discipline. You’ll be kicked from the training if you misbehave.
* Once a week at most, a Hardcore Training will be hosted. In this training, even the slightest mistake will get you kicked. If you ‘survive’ to the end, you can earn up to 20 points.
* While anyone can assist a training, **do not ask to be an assistant.**
* You can use the command ```!traininginfo``` at any PB facility to get info about frequently used terms in trainings, and information about PTS.

**Different types of training:**
* Normal Traning: These are the most common type of training. For each normal training, depending on your performance and behaviour, you can earn up to 5 points.
* Mega Training: These types of training happen mostly every Saturday at 5:00 PM UTC. Times can change under rare occassuions. You can earn double the points of normal training here.
* Disciplinary Training: These types of training are more stricter than normal trainings and requires you to be more focused. If you break the rules, you have a higher chance of getting kicked instead on getting warned like in normal training.
* Hardcore Training: The slightest mistake will get you kicked in these types of training. If you "survive" to the end, you can earn up to 20 points and earn a "Hardcore Training Survivor" role on the PBST discord.
* Lecture: This is where the host will explain the contents presented within the handbook. You can ask the trainer for questions relating to the handbook.
* Evaluation: These types of trainings are exclusive to cadets only and will provide them with a chance to obtain the tier 1 role. They'll be tested on a series of theory and practical tasks which prepares them for becoming a tier. Completing and passing this training will allow you to be given the tier 1 role.

Keep an eye on the scheduler to know when and where these trainings will happen.

More information about points can be found [here](../ranks-and-ranking-up/)
More information about the ranks in PBST can be found [here](../ranks-and-ranking-up/#ranks)

::: warning NOTE:
These rules can be changed at any time. So please check every now and then for a update on the handbook.
:::

